package org.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
public class JpaRepositoriesConfig {

    @Configuration
    @EnableJpaRepositories(entityManagerFactoryRef = "emf_one", transactionManagerRef = "tx_manager_one",
            basePackages = "org.test.repository.one")
    static class DbOnepaRepositoriesConfig {
    }

    @Configuration
    @EnableJpaRepositories(entityManagerFactoryRef = "emf_two", transactionManagerRef = "tx_manager_two",
            basePackages = "org.test.repository.two")
    static class DbTwoJpaRepositoriesConfig {
    }
}