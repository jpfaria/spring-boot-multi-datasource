package org.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class JpaConfig {

    private static final String packagesToScan = "org.test.entity";

    @Autowired
    @Qualifier("ds_one")
    private DataSource dsOne;

    @Autowired
    @Qualifier("ds_two")
    private DataSource dsTwo;

    @Autowired
    private PersistenceProvider persistenceProvider;

    @Autowired
    private JpaVendorAdapter jpaAdapter;

    @Autowired
    @Qualifier("jpa_prop_one")
    private PropertiesFactoryBean jpaPropertiesOne;

    @Autowired
    @Qualifier("jpa_prop_two")
    private PropertiesFactoryBean jpaPropertiesTwo;

    @Bean(name = "emf_one")
    LocalContainerEntityManagerFactoryBean entityManagerFactoryOne() throws IOException {

        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dsOne);
        emf.setPackagesToScan(packagesToScan);
        emf.setPersistenceProvider(persistenceProvider);
        emf.setJpaProperties(jpaPropertiesOne.getObject());
        //emf.setPersistenceUnitName("dbOne");
        emf.setJpaVendorAdapter(jpaAdapter);
        return emf;
    }

    @Bean(name = "tx_manager_one")
    PlatformTransactionManager transactionManagerOne() throws IOException {
        return new JpaTransactionManager(entityManagerFactoryOne().getObject());
    }

    @Bean(name = "emf_two")
    LocalContainerEntityManagerFactoryBean entityManagerFactoryTwo() throws IOException {

        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dsTwo);
        emf.setPackagesToScan(packagesToScan);
        emf.setPersistenceProvider(persistenceProvider);
        emf.setJpaProperties(jpaPropertiesTwo.getObject());
        //emf.setPersistenceUnitName("dbTwo");
        emf.setJpaVendorAdapter(jpaAdapter);
        return emf;
    }

    @Bean(name = "tx_manager_two")
    PlatformTransactionManager transactionManagerTwo() throws IOException {
        return new JpaTransactionManager(entityManagerFactoryTwo().getObject());
    }


}
