package org.test.repository.one;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.entity.Pessoa;

public interface PessoaDbOneJpaRepository extends JpaRepository<Pessoa, Integer> {
}
