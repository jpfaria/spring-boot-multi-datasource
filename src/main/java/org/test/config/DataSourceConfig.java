package org.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.test.property.BaseDataSourceProperty;

import javax.sql.DataSource;
import java.sql.Driver;

@Configuration
@EnableConfigurationProperties({
        DataSourceConfig.DataSourceOneProperty.class,
        DataSourceConfig.DataSourceTwoProperty.class})
public class DataSourceConfig {

    @Autowired
    DataSourceOneProperty dsOneProperties;

    @Autowired
    DataSourceTwoProperty dsTwoProperties;

    @Bean
    @Qualifier("ds_one")
    public DataSource dataSourceOne() throws ClassNotFoundException {
        return createDataSource(dsOneProperties);
    }

    @Bean
    @Qualifier("ds_two")
    public DataSource dataSourceTwo() throws ClassNotFoundException {
        return createDataSource(dsTwoProperties);
    }

    private DataSource createDataSource(BaseDataSourceProperty properties) throws ClassNotFoundException {
        SimpleDriverDataSource ds = new SimpleDriverDataSource();
        ds.setDriverClass((Class<? extends Driver>) Class.forName(properties.getDriverClassName()));
        ds.setUrl(properties.getUrl());
        ds.setUsername(properties.getUsername());
        ds.setPassword(properties.getPassword());
        return ds;
    }

    @ConfigurationProperties(prefix = "appName.dbOne.datasource")
    static class DataSourceOneProperty extends BaseDataSourceProperty {
    }

    @ConfigurationProperties(prefix = "appName.dbTwo.datasource")
    static class DataSourceTwoProperty extends BaseDataSourceProperty {
    }

}