package org.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.test.dto.PessoaDto;
import org.test.service.PessoaService;

import java.util.List;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @RequestMapping(value = "/one", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<PessoaDto> pessoasDb1() {
        return pessoaService.getPessoasFromDbOne();
    }

    @RequestMapping(value = "/two", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<PessoaDto> pessoasDb2() {
        return pessoaService.getPessoasFromDbTwo();
    }

}
