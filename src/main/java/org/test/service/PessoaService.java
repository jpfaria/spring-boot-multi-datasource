package org.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.test.dto.PessoaDto;
import org.test.entity.Pessoa;
import org.test.repository.one.PessoaDbOneJpaRepository;
import org.test.repository.two.PessoaDbTwoJpaRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private PessoaDbOneJpaRepository pessoaDbOneJpaRepository;

    @Autowired
    private PessoaDbTwoJpaRepository pessoaDbTwoJpaRepository;

    public List<PessoaDto> getPessoasFromDbOne() {
        List<Pessoa> pessoas = pessoaDbOneJpaRepository.findAll();
        return createPessoasDto(pessoas);
    }

    public List<PessoaDto> getPessoasFromDbTwo() {
        List<Pessoa> pessoas = pessoaDbTwoJpaRepository.findAll();
        return createPessoasDto(pessoas);
    }

    private List<PessoaDto> createPessoasDto(List<Pessoa> pessoas) {
        List<PessoaDto> pessoasDto = new ArrayList<>();

        for (Pessoa pessoa : pessoas) {
            PessoaDto pessoaDto = PessoaDto.builder()
                    .nome(pessoa.getNome())
                    .ddd(pessoa.getTelefone().getDdd())
                    .telefone(pessoa.getTelefone().getNumero())
                    .build();

            pessoasDto.add(pessoaDto);
        }

        return pessoasDto;
    }

}
