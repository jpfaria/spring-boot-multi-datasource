package org.test.repository.two;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.entity.Pessoa;

public interface PessoaDbTwoJpaRepository extends JpaRepository<Pessoa, Integer> {
}
