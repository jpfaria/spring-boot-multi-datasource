package org.test.property;

import lombok.Data;

@Data
public class BaseHibernateProperty {

    protected String hbm2ddlAuto;
    protected String hbm2ddlImportFiles;
    protected Boolean showSql;
    protected String dialect;
    protected Boolean generateStatistics;
    protected Boolean useSecondLevelCache;
    protected Boolean useQueryCache;
    protected Boolean useStructuredEntries;
    protected Boolean formatSql;

}