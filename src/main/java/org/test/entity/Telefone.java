package org.test.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "T_TELEFONE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Telefone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "telefone")
    private Pessoa pessoa;

    private Integer ddd;

    private Integer numero;


}
