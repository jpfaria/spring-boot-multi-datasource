package org.test.property;


import lombok.Data;

@Data
abstract public class BaseDataSourceProperty {

    protected String driverClassName;
    protected String url;
    protected String username;
    protected String password;

}
