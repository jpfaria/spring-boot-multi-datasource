package org.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class EntityManagerInViewConfig extends WebMvcConfigurerAdapter {

    @Autowired
    @Qualifier("emf_one")
    LocalContainerEntityManagerFactoryBean entityManagerFactoryOne;

    @Autowired
    @Qualifier("emf_two")
    LocalContainerEntityManagerFactoryBean entityManagerFactoryTwo;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        OpenEntityManagerInViewInterceptor interceptorOne = new OpenEntityManagerInViewInterceptor();
        interceptorOne.setEntityManagerFactory(entityManagerFactoryOne.getObject());
        registry.addWebRequestInterceptor(interceptorOne);

        OpenEntityManagerInViewInterceptor interceptorTwo = new OpenEntityManagerInViewInterceptor();
        interceptorTwo.setEntityManagerFactory(entityManagerFactoryTwo.getObject());
        registry.addWebRequestInterceptor(interceptorTwo);
    }

}
