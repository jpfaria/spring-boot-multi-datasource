package org.test.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.test.property.BaseHibernateProperty;

import javax.persistence.spi.PersistenceProvider;
import java.util.Properties;

@Configuration
@EnableConfigurationProperties({
        JpaHibernateConfig.HibernateOneProperty.class,
        JpaHibernateConfig.HibernateTwoProperty.class})
public class JpaHibernateConfig {

    private static final String PROPERTY_HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String PROPERTY_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String PROPERTY_HIBERNATE_HBM2DDL_IMPORT_FILES = "hibernate.hbm2ddl.import_files";
    private static final String PROPERTY_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String PROPERTY_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String PROPERTY_GENERATE_STATISTICS = "hibernate.generate_statistics";
    private static final String PROPERTY_CACHE_USE_SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";
    private static final String PROPERTY_CACHE_USE_QUERY_CACHE = "hibernate.cache.use_query_cache";
    private static final String PROPERTY_CACHE_USE_STRUCTURED_ENTRIES = "hibernate.cache.use_structured_entries";

    @Autowired
    HibernateOneProperty hibernateOneProperties;

    @Autowired
    HibernateTwoProperty hibernateTwoProperties;

    @Bean
    public PersistenceProvider persistenceProvider() {
        return new HibernatePersistenceProvider();
    }

    @Bean
    public JpaVendorAdapter jpaAdapter() {
        HibernateJpaVendorAdapter jpaAdapter = new HibernateJpaVendorAdapter();
        return jpaAdapter;
    }

    @Bean
    @Qualifier("jpa_prop_one")
    public PropertiesFactoryBean jpaPropertiesOne() {
        return createProperties(hibernateOneProperties);
    }

    @Bean
    @Qualifier("jpa_prop_two")
    public PropertiesFactoryBean jpaPropertiesTwo() {
        return createProperties(hibernateTwoProperties);
    }

    private PropertiesFactoryBean createProperties(BaseHibernateProperty hibernateProperties) {

        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();

        Properties properties = new Properties();

        properties.setProperty(PROPERTY_HIBERNATE_DIALECT, hibernateProperties.getDialect());
        properties.setProperty(PROPERTY_HIBERNATE_SHOW_SQL, hibernateProperties.getShowSql().toString());
        properties.setProperty(PROPERTY_HIBERNATE_HBM2DDL_AUTO, hibernateProperties.getHbm2ddlAuto());
        properties.setProperty(PROPERTY_HIBERNATE_HBM2DDL_IMPORT_FILES, hibernateProperties.getHbm2ddlImportFiles());
        properties.setProperty(PROPERTY_HIBERNATE_FORMAT_SQL, hibernateProperties.getFormatSql().toString());
        properties.setProperty(PROPERTY_GENERATE_STATISTICS, hibernateProperties.getGenerateStatistics().toString());
        properties.setProperty(PROPERTY_CACHE_USE_SECOND_LEVEL_CACHE, hibernateProperties.getUseSecondLevelCache().toString());
        properties.setProperty(PROPERTY_CACHE_USE_QUERY_CACHE, hibernateProperties.getUseQueryCache().toString());
        properties.setProperty(PROPERTY_CACHE_USE_STRUCTURED_ENTRIES, hibernateProperties.getUseStructuredEntries().toString());

        propertiesFactoryBean.setProperties(properties);

        return propertiesFactoryBean;
    }

    @ConfigurationProperties(prefix = "appName.dbOne.hibernate")
    static class HibernateOneProperty extends BaseHibernateProperty {
    }

    @ConfigurationProperties(prefix = "appName.dbTwo.hibernate")
    static class HibernateTwoProperty extends BaseHibernateProperty {
    }


}
